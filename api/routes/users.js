const express = require('express');
const router = express.Router();

const userController = require('../controllers/users');

router.get('/', userController.getAll);
router.post('/signup', userController.userSignup);
router.post('/login', userController.userLogin);
router.delete('/:userId', userController.deleteById);

module.exports = router;