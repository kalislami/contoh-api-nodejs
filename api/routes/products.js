const express = require('express');
const router = express.Router();
const multer = require('multer');

const checkAuth = require('../middleware/check-auth');
const productController = require('../controllers/products');

const fileFilter = (req, file, callback) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg' || file.mimetype === 'image/png') {
        callback(null, true);
    } else {
        callback(null, false);
    }
};
const storage = multer.diskStorage({
    destination: function(req, file, callback){
        callback(null, './uploads/');
    },
    filename: function(req, file, callback){
        callback(null, new Date().toISOString() + file.originalname);
    }
});
const upload = multer({ 
    storage: storage, 
    limits: {
        fileSize: 1024 * 1024 * 3 // 3mb
    },
    fileFilter: fileFilter
});

router.get('/', checkAuth, productController.getAll);
router.post('/', checkAuth, upload.single('productImage'), productController.addProduct);
router.get('/:productId', checkAuth, productController.getById);
router.patch('/:productId', checkAuth, productController.updateProduct);
router.delete('/:productId', checkAuth, productController.deleteById);

module.exports = router;