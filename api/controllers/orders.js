const mongoose = require('mongoose');

const Order = require('../model/order');
const Product = require('../model/product');

exports.getAll = (req, res, next) => {
    Order
        .find()
        .select('_id quantity product')
        .populate('product', 'name price')
        .exec()
        .then(docs => {
            const count = docs.length;
            res.status(200).json({
                count: count,
                orders: docs.map(doc => {
                    return {
                        id: doc._id,
                        quantity: doc.quantity,
                        product: doc.product
                    }
                })
            })
        })
        .catch(err => {
            res.status(500).json(err);
        })
};

exports.addOrder = (req, res, next) => {
    Product
        .findById(req.body.productId)
        .then(product => {
            if (!product) {
                res.status(404).json({
                    message: "unknown productId"
                })
            } else {
                const order = new Order({
                    _id: mongoose.Types.ObjectId(),
                    product: req.body.productId,
                    quantity: req.body.quantity
                });
                
                order
                    .save()
                    .then(result => {
                        res.status(200).json({
                            message: "order created",
                            order: result
                        });
                    })
                    .catch(err => {
                        res.status(500).json(err);
                    });
            }
        })
        .catch(err => {
            res.status(404).json({
                message: "unkown productId"
            });
        });
};

exports.getById = (req, res, next) => {
    Order.findById(req.params.orderId)
        .exec()
        .then(order => {
            if (!order) {
                res.status(404).json({
                    message: "order not found"
                })
            }
            res.status(200).json(order);
        })
        .catch(err => {
            res.status(500).json(err);
        });
};

exports.deleteById = (req, res, next) => {
    Order.remove({ _id: req.params.orderId })
        .exec()
        .then(result => {
            res.status(200).json({
                message: "order deleted"
            })
        })
        .catch(err => {
            res.status(500).json(err)
        });
};