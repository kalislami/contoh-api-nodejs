const mongoose = require('mongoose');

const Product = require('../model/product');

exports.getAll = (req, res, next) => {
    Product.find()
        .select('name price _id productImage')
        .exec()
        .then(docs => {
            const data = {
                count: docs.length,
                data: docs.map(doc => {
                    return {
                        id: doc._id,
                        name: doc.name,
                        price: doc.price,
                        productImage: doc.productImage,
                        request: {
                            type: 'GET',
                            url: 'http://localhost:3000/products/' + doc._id
                        }
                    }
                })
            }
            res.status(200).json(data);
        })
        .catch(err => {
            console.log(err);
            res.status(500).json(err);
        })
};

exports.addProduct = (req, res, next) => {
    console.log(req.file);
    const product = new Product({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        price: req.body.price,
        productImage: req.file.path
    });
    
    product
        .save()
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: 'new product created successfuly',
                createdProduct: {
                    id: result._id,
                    name: result.name,
                    price: result.price,
                    request: {
                        type: 'GET',
                        url: 'http://localhost:3000/products' + result._id
                    }
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json(err);
        });
};

exports.getById = (req, res, next) => {
    const id = req.params.productId;

    Product.findById(id)
        .select('_id name price productImage')
        .exec()
        .then(doc => {
            res.status(200).json({
                product: doc,
                request: {
                    type: 'GET',
                    url: 'http://localhost/products'
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(404).json({
                message: err
            })
        })
};

exports.updateProduct = (req, res, next) => {
    const id = req.params.productId;
    const updateOps = {};
    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value;
    }
    
    Product.update({ _id: id }, { $set: updateOps })
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'successfully updated product',
                request: {
                    type: 'GET',
                    url: 'http://localhost:3000/products/' + id
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json(err);
        });
};

exports.deleteById = (req, res, next) => {
    const id = req.params.productId;

    Product.remove({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'succesfully deleted product'
            });
        }).catch(err => {
            console.log(err);
            res.status(500).json(err);
        });
};