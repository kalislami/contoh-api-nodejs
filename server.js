const http = require('http');
const app = require('./app');
const port = process.env.PORT || 3333;
const server = http.createServer(app);
const mongoose = require('mongoose');

const user = process.env.DB_USER;
const password = process.env.DB_PASSWORD;
const db = process.env.DB_NAME;
const url = `mongodb+srv://${user}:${password}@kalislami-ehjff.mongodb.net/${db}?retryWrites=true&w=majority`;

mongoose.Promise = global.Promise;
mongoose.connect(url, { useNewUrlParser: true, useCreateIndex: true }
).then(() => {
    server.listen(port, () => {
        console.log('listening port: '+port);
    });
    console.log('connected to database yeaah!!');
}).catch(err => {
    console.log('errornya kenapa? '+err);
});